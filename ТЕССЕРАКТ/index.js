import { createWorker, PSM } from 'tesseract.js';


/*
// УСЛОВНОСТИ
/*
Лучший вариант для нас
1) Черный текст на белом фоне
2) Шрифт легко читается
3) Иногда следует включить чуть больше фона в исходное изображение. Если текст занимает все пространство
это может привести к проблемам
4) На картинке один балун (нет пустого пространства между балунами). Это самый простой вариант.
Я нашел инфу как это пофиксить
5) Результат сильно зависит от качества изображения. Чем текст четче, тем лучше
6) Мусор и кривота будут всегда. Нужно с этим смириться
 */


/*
//ПРО МОДЫ
// PSM.AUTO_ONLY круто работает
// PSM.AUTO не особо понял разницу с PSM.AUTO_ONLY
// PSM.SINGLE_BLOCK зачастую находит кучу несуществующих символов
// PSM_SPARSE_TEXT неплохо подходит для одиночных балунов. Если текст разделен, он выводится беспорядочно
//
// моды ниже очень хорошо определяют слова
//PSM_AUTO_ONLY,      ///< Automatic page segmentation, but no OSD, or OCR.
//PSM_AUTO,           ///< Fully automatic page segmentation, but no OSD.
// не совсем понял чем PSM_AUTO_ONLY и PSM_AUTO отличаются
//PSM_SPARSE_TEXT неплохо подходит для одиночных балунов. Если текст разделен, он выводится беспорядочно

//
*/
// удаляет лишние пробелы в строке и возвращает массив осмысленных слов.
// в конце каждого слова есть пробел, чтобы можно было легко сделать join
function clean(str) {
    return str.split(' ').filter(word => word !== '');
}

let path = `images/kek7.jpg`


// осторожно. Удалять пробел категорически нельзя
// возможно это стоит сделать через регулярку
const alphabetStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890,.'-?! ";

const worker = createWorker({
    logger: m => console.log(m)
});

async function getWordsArray(path) {
    let newStr = ''
    let options = {
        tessedit_char_whitelist: alphabetStr,
        tessedit_pageseg_mode: PSM.AUTO,
        tessjs_create_hocr: `0`,
        tessjs_create_tsv: `0`,
    }
    await worker.load();
    await worker.loadLanguage('eng');
    await worker.initialize('eng');
    await worker.setParameters(options);
    const dataObj = await worker.recognize(path);

    dataObj.data.words.forEach(word => {
        // word.confidence>=50 этот значение показалось мне оптимальным, чтобы оставлять осмысленные слова, но по идее его можно регулировать
        // иногда возникает ситуация, когда слово распозналось верно, но конфиденс равен 0,
        if (word.confidence>=50) {
            newStr += word.text + ' ';
        }
    })
    await worker.terminate();
    console.log(clean(newStr))
    console.log(clean(newStr).join(' '))
    // return clean(newStr)
}

getWordsArray(path)